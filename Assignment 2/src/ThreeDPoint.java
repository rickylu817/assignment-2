/**
 * An unmodifiable point in the three-dimensional space. The coordinates are specified by exactly three doubles (its
 * <code>x</code>, <code>y</code>, and <code>z</code> values).
 */
public class ThreeDPoint implements Point
{
    //Instance variables
    private double xValue;
    private double yValue;
    private double zValue;
    
    public ThreeDPoint(double x, double y, double z)
    {
        //Setting the values
        xValue = x;
        yValue = y;
        zValue = z;
    }

    //Accessor methods
    public double getX()
    {
        return xValue;
    }
    
    public double getY()
    {
        return yValue;
    }
    
    public double getZ()
    {
        return zValue;
    }
    
    //Mutator methods
    public void setX(int x)
    {
        xValue = x;
    }
    
    public void setY(int y)
    {
        yValue = y;
    }
    
    public void setZ(int z)
    {
        zValue = z;
    }
    
    /**
     * @return the (x,y,z) coordinates of this point as a <code>double[]</code>.
     */
    @Override
    public double[] coordinates()
    {
        double output[] = {xValue,yValue,zValue};
        
        return output;
    }
    
    public String toString()
    {
        return "X="+xValue+", "+"Y="+yValue+", "+"Z="+zValue;
    }
    
    public static double findLength(ThreeDPoint a, ThreeDPoint b)
    {
        double output = 0;
        
        double ax = a.getX();
        double ay = a.getY();
        double az = a.getZ();
        
        double bx = b.getX();
        double by = b.getY();
        double bz = b.getZ();
        
        output = Math.sqrt(Math.pow(ax-bx,2)+Math.pow(ay-by,2)+Math.pow(az-bz,2));
        
        return output;
    }
}
