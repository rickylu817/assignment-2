import java.util.*;

public class OrderingBackup
{
    public static void main(String[] args)
    {
        List<ThreeDShape> shapes = new ArrayList<>();
        Sphere s1 = Sphere.random();
        List<ThreeDPoint> list = new ArrayList<>();
        
        list.add(new ThreeDPoint(100,63,-59));
        list.add(new ThreeDPoint(86,63,-59));
        list.add(new ThreeDPoint(86,-9,-59));
        list.add(new ThreeDPoint(100,-9,-59));
        list.add(new ThreeDPoint(100,-9,-108));
        list.add(new ThreeDPoint(100,63,-108));
        list.add(new ThreeDPoint(86,63,-108));
        list.add(new ThreeDPoint(86,-9,-108));
        
        Cuboid c1 = new Cuboid(list);
        
        shapes.add(s1);
        shapes.add(c1);
        shapes.add(Sphere.random());
        shapes.add(Cuboid.random());
        shapes.add(Sphere.random());
        shapes.add(Cuboid.random());
        shapes.add(Cuboid.random());
        
        /**
        System.out.println(shapes);
        System.out.println("After");
        Collections.sort(shapes,new Ordering.SurfaceAreaComparator());
        System.out.println(shapes);
        System.out.println("----------Alpha");
        **/
        
        List<TwoDShape> twod = new ArrayList<TwoDShape>();
        
        Quadrilateral q1 = new Quadrilateral(0,1,-2,0,-1,-1,1,0);
        System.out.println(q1);
        
        Quadrilateral q2 = new Rectangle(2.5,2.5,0,2.5,0,-2.5,2.5,-2.5);
        System.out.println(q2);
        
        Square sq1 = new Square(TwoDPoint.ofDoubles(5,5,3,5,3,3,5,3));
        System.out.println(sq1);
        
        Square sq2 = new Square(TwoDPoint.ofDoubles(-5,-5,-3,-5,-3,-3,-5,-3));
        System.out.println(sq2);
        
        TwoDShape t1 = new Circle(1,2,3);
        System.out.println(t1);
        System.out.println("Testing Phase\n");
        twod.add(q1);
        twod.add(q2);
        twod.add(sq1);
        twod.add(sq2);
        twod.add(t1);
        
        printList(twod);
        Collections.sort(twod,new Ordering.XLocationComparator());
        System.out.println("After sorting");
        printList(twod);
        
        System.out.println("----------Beta");
        
        List<SymmetricTwoDShape> list2 = new ArrayList<>();
        
        SymmetricTwoDShape st1 = new Rectangle(1,1,0,1,0,0,1,0);
        SymmetricTwoDShape st2 = new Circle(1,2,4);
        SymmetricTwoDShape st3 = new Square(TwoDPoint.ofDoubles(10,3,7,3,7,0,10,0));
        System.out.println(st1);
        System.out.println(st2);
        System.out.println(st3);
        list2.add(st1);
        list2.add(st2);
        list2.add(st3);
        
        printList(list2);
        System.out.println("After sorting");
        Collections.sort(list2,new Ordering.AreaComparator());
        printList(list2);
        
    }
    
    private static <T> void printList(Collection<T> items)
    {
        for(T a:items)
        {
            System.out.println(a);
        }
    }
}
