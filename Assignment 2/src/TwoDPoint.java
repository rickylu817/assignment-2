import java.util.ArrayList;
import java.util.List;

/**
 * An unmodifiable point in the standard two-dimensional Euclidean space. The
 * coordinates of such a point is given by exactly two doubles specifying its
 * <code>x</code> and <code>y</code> values.
 */
public class TwoDPoint implements Point
{
    //Private variables
    private double xValue;
    private double yValue;
    
    public TwoDPoint(double x, double y)
    {
        xValue = x;
        yValue = y;
    }
    
    //Accesor methods
    public double getX()
    {
        return xValue;
    }
    
    public double getY()
    {
        return yValue;
    }
    
    //Mutator methods
    public void setX(double x)
    {
        xValue = x;
    }
    
    public void setY(double y)
    {
        yValue = y;
    }

    /**
     * @return the coordinates of this point as a <code>double[]</code>.
     */
    @Override
    public double[] coordinates()
    {
        double output[] = {xValue,yValue};
        
        return output;
    }

    /**
     * Returns a list of <code>TwoDPoint</code>s based on the specified array of
     * doubles. A valid argument must always be an even number of doubles so
     * that every pair can be used to form a single <code>TwoDPoint</code> to be
     * added to the returned list of points.
     *
     * @param coordinates the specified array of doubles.
     * @return a list of two-dimensional point objects.
     * @throws IllegalArgumentException if the input array has an odd number of
     * doubles.
     */
    public static List<TwoDPoint> ofDoubles(double... coordinates) throws IllegalArgumentException
    {
        List<TwoDPoint> output = new ArrayList<TwoDPoint>();
        
        //That means the list of coordinates are odd therefore we throw an
        //illegalArgumentException
        if(coordinates.length%2==1)
        {
            throw new IllegalArgumentException("Input array has an odd number of doubles");
        }
        
        //If we are outside then that means coordinates have even number of doubles
        //Thus we can return a list of TwoDPoint based on the list of doubles
        //We have to stop this for loop one early because we are getting every pair
        for(int i=0;i<coordinates.length;i=i+2)
        {
            //Getting the two pair of doubles as x and y
            double xPoint = coordinates[i];
            double yPoint = coordinates[i+1];
            
            //Preparing the TwoDPoint object to be added to output
            TwoDPoint toBeAdded = new TwoDPoint(xPoint,yPoint);
            
            //Adding it into output
            output.add(toBeAdded);
        }
        
        //If we are out here that means output have the list of TwoDPoints
        return output;
    }
    
    public String toString()
    {
        return xValue+", "+yValue;
    }
}
