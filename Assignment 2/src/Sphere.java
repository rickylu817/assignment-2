public class Sphere implements ThreeDShape
{
    //Instance variables
    private ThreeDPoint center;
    private double radius;
    
    //Constructor for the Sphere object
    public Sphere(double x,double y,double z,double givenRadius)
    {
        center = new ThreeDPoint(x,y,z);
        radius = givenRadius;
    }
    
    //Accessor method
    public double getRadius()
    {
        return radius;
    }
    
    @Override
    public Point center()
    {
        return center;
    }

    //Mutator methods
    public void setRadius(double givenRadius)
    {
        radius = givenRadius;
    }
    
    public void setCenter(ThreeDPoint givenPoint)
    {
        center = givenPoint;
    }
    
    @Override
    public double volume()
    {
        //Computing the volume for a Sphere with this radius
        double output = ((double)4/3)*Math.PI*Math.pow(radius, 3);
        
        return output;
    }

    @Override
    public int compareTo(ThreeDShape o)
    {
        //This is the volume of this Sphere
        double thisVolume = this.volume();
        
        //This is the volume of that Sphere
        double thatVolume = o.volume();
        
        //Same volume then return 0
        if(thisVolume==thatVolume)
        {
            return 0;
        }
        //If this volume is greater than that volume you comparing to return 1
        else if(thisVolume>thatVolume)
        {
            return 1;
        }
        //If it is not then return -1
        else
        {
            return -1;
        }
    }
    
    //toString method that return a nice String representation of the Sphere object
    public String toString()
    {
        String output = center+"\nRadius: "+radius+"\nSurface Area: "+surfaceArea()+"\nVolume: "+volume()+"\n";
        
        return output;
    }
    
    //This method will allow us to find the surface area of this Sphere
    public double surfaceArea()
    {
        //Commputing the surface area using the forumla
        double output = 4*Math.PI*Math.pow(radius, 2);
        
        return output;
    }
    
    //Create a random sphere object
    public static Sphere random()
    {
        //The output will be holding our random Sphere
        Sphere output;
        
        //Generating a random center with the range -100 to 100
        double randomX = (int)(Math.random()*201-100);
        double randomY = (int)(Math.random()*201-100);
        double randomZ = (int)(Math.random()*201-100);
        
        //Generating a random radius with the range 1 to 100
        double radius = (int)(Math.random()*100+1);
        
        //Making our random Sphere
        output = new Sphere(randomX,randomY,randomZ,radius);
        
        return output;
    }
    
}