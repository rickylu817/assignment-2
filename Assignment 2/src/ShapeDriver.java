import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ShapeDriver
{
    public static void main(String [] args)
    {
        List<ThreeDPoint> points = new ArrayList<ThreeDPoint>();
        
        points.add(new ThreeDPoint(-2.29,-0.06,2));
        points.add(new ThreeDPoint(-0.32,2.31,3.73));
        points.add(new ThreeDPoint(1.97,2.37,1.05));
        points.add(new ThreeDPoint(0,0,-0.69));
        
        points.add(new ThreeDPoint(1.83,-2.62,0.81));
        points.add(new ThreeDPoint(-0.46,-2.68,3.5));
        points.add(new ThreeDPoint(1.51,-0.32,5.24));
        points.add(new ThreeDPoint(3.9,-0.25,2.55));
        
        Cuboid shape = new Cuboid(points);
        System.out.println(shape.volume());
        System.out.println("The center is at "+shape.center());
        
        Quadrilateral q1 = new Quadrilateral(3.82,2.17,-2.36,2.15,-3.6,-1.05,2.16,-1.07);
        double lengths[] = q1.getSideLengths();
        for(double a:lengths)
        {
            System.out.println(a);
        }
        System.out.println("--------k");
        
        List<TwoDPoint> newList = new ArrayList<TwoDPoint>();
        newList.add(new TwoDPoint(3.82,2.17));
        newList.add(new TwoDPoint(0.54,2.17));
        newList.add(new TwoDPoint(-1,-1));
        newList.add(new TwoDPoint(2.16,-1.07));
        
        q1.setPosition(newList);
        
        lengths = q1.getSideLengths();
        for(double a:lengths)
        {
            System.out.println(a);
        }
        
        System.out.println("-----------------");
        Rectangle r1 = new Rectangle(TwoDPoint.ofDoubles(7.34,2.68,4.34,2.68,4.34,0,7.34,0));
        Square s1 = new Square(TwoDPoint.ofDoubles(2,2,0,2,0,0,2,0));
        System.out.println(r1.center());
        System.out.println(r1.area());
        System.out.println(s1.area());
        
        System.out.println("----------------Bubble");
        
        Square s2 = new Square(TwoDPoint.ofDoubles(0.4,0.4,0,0.4,0,0,0.4,0));
        System.out.println(s2.snap());
        
        List<Sphere> list = new ArrayList<Sphere>();
        Collections.sort(list);
        printList(list);
        
        System.out.println("-----------------------");
        List<ThreeDPoint> p2 = new ArrayList<>();
        p2.add(new ThreeDPoint(3,3,3));
        p2.add(new ThreeDPoint(0,3,3));
        p2.add(new ThreeDPoint(0,0,3));
        p2.add(new ThreeDPoint(3,0,3));
        p2.add(new ThreeDPoint(3,0,0));
        p2.add(new ThreeDPoint(3,3,0));
        p2.add(new ThreeDPoint(0,3,0));
        p2.add(new ThreeDPoint(0,0,0));
        
        Cuboid c1 = new Cuboid(p2);
        System.out.println(c1);
        
        
        Cuboid c2 = Cuboid.random();
        System.out.println(c2);
        
        Sphere s3 = Sphere.random();
        System.out.println(s3);
        System.out.println("-------------------Big Loop");
        
        List<TwoDShape> t1 = new ArrayList<TwoDShape>();
        List<SymmetricTwoDShape> t2 = new ArrayList<>();
        
        t2.add(new Circle(1,2,3));
        t2.add(new Square(TwoDPoint.ofDoubles(0.4,0.4,0,0.4,0,0,0.4,0)));
        printList(t2);
        System.out.println("---------------Hello There!");
        System.out.println(t2.get(1).center());
        
        Ordering.copy(t2,t1);
        printList(t1);
        

        System.out.println("-------------Beta");
        Rectangle r2 = new Rectangle(TwoDPoint.ofDoubles(5,8,0,8,0,0,5,0));
        System.out.println(r2);
        System.out.println(r2.center());
        
        Square test2 = new Square(TwoDPoint.ofDoubles(-1.1235, 4.1235, -1.1235, 0.1235,
                2.8765, 0.1235, 2.8765, 4.1235));
        
        System.out.println(test2);
        System.out.println(test2.center());
        
    }
    
    public static <T> void printList(List<T> list)
    {
        for(T a:list)
        {
            System.out.println(a);
        }
    }
}