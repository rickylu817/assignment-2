import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;

public class Square extends Rectangle implements Snappable
{
    //This constructor will call on the Rectangle's constructor to make a square
    public Square(List<TwoDPoint> vertices)
    {
        super(vertices);
    }

    /**
     * Given a list of vertices assumed to be provided in a counterclockwise
     * order in a two-dimensional plane, checks whether or not they constitute a
     * valid square.
     *
     * @param vertices the specified list of vertices in a counterclockwise
     * order
     * @return <code>true</code> if the four vertices can form a square,
     * <code>false</code> otherwise.
     */
    @Override
    public boolean isMember(List<? extends Point> vertices)
    {
        return vertices.size() == 4
                && DoubleStream.of(getSideLengths()).boxed().collect(Collectors.toSet()).size() == 1;
    }

    /**
     * Snaps the sides of the square such that each corner (x,y) is modified to
     * be a corner (x',y') where x' is the the integer value closest to x and y'
     * is the integer value closest to y. This, of course, may change the shape
     * to a general quadrilateral, hence the return type. The only exception is
     * when the square is positioned in a way where this approximation will lead
     * it to vanish into a single point. In that case, a call to {@link #snap()}
     * will not modify this square in any way.
     */
    @Override
    public Quadrilateral snap()
    {
        //This is the output quadrilateral
        Quadrilateral output;
        
        //Getting the list of point of this square
        List<TwoDPoint> points = this.getPosition();
        
        //This list will be storing all of the rounded points
        List<TwoDPoint> roundedPoints = new ArrayList<>();
        
        //This for loop will be responsible of going through the list of points and
        //making each new points to be used as output
        for(int i=0;i<points.size();i++)
        {
            TwoDPoint pointI = points.get(i);
            
            //Getting the x and y value itself
            double xValue = pointI.getX();
            double yValue = pointI.getY();
            
            //Rounding it to the closest integer value
            double roundedX = Math.round(xValue);
            double roundedY = Math.round(yValue);
            
            //Then constructing one TwoDPoint with the rounded x and y
            TwoDPoint roundedPoint = new TwoDPoint(roundedX,roundedY);
            
            //Putting it inside the roundedPoints
            roundedPoints.add(roundedPoint);
        }
        
        //We make the output, but we can't return it just yet
        output = new Quadrilateral(roundedPoints);
        
        //Then after doing the roundedPoints we have to check whether or not they are
        //all equal if they converged all into one point
        //Point a will be the point that we contrast with the rest of the points
        TwoDPoint a = roundedPoints.get(0);
        
        for(int i=1;i<roundedPoints.size();i++)
        {
            //Getting the TwoDPoint at i
            TwoDPoint pointI = roundedPoints.get(i);
            
            //If one of this is true then it didn't converge to one point
            if(a.getX()!=pointI.getX()||a.getY()!=pointI.getY())
            {
                return output;
            }
        }
        
        //If we are out here then that means all of the points are equal to each other
        //Therefore we will just return the original square
        return this;
    }
}
