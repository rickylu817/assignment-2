import java.util.ArrayList;
import java.util.List;

// TODO : a missing interface method must be implemented in this class to make it compile. This must be in terms of volume().
public class Cuboid implements ThreeDShape
{
    private final ThreeDPoint[] vertices = new ThreeDPoint[8];

    /**
     * Creates a cuboid out of the list of vertices. It is expected that the
     * vertices are provided in the order as shown in the figure given in the
     * homework document (from v0 to v7).
     *
     * @param vertices the specified list of vertices in three-dimensional
     * space.
     */
    public Cuboid(List<ThreeDPoint> vertices)
    {
        if(vertices.size() != 8)
        {
            throw new IllegalArgumentException(String.format("Invalid set of vertices specified for %s",
                    this.getClass().getName()));
        }
        int n = 0;
        for (ThreeDPoint p : vertices)
        {
            this.vertices[n++] = p;
        }
    }

    @Override
    public double volume()
    {
        //Output will store the answers
        double output = 0;
        
        //Getting the vertices that is needed for calculations
        ThreeDPoint v2 = vertices[2];
        double v2x = v2.getX();
        double v2y = v2.getY();
        double v2z = v2.getZ();
        ThreeDPoint v3 = vertices[3];
        double v3x = v3.getX();
        double v3y = v3.getY();
        double v3z = v3.getZ();
        ThreeDPoint v4 = vertices[4];
        double v4x = v4.getX();
        double v4y = v4.getY();
        double v4z = v4.getZ();
        ThreeDPoint v5 = vertices[5];
        double v5x = v5.getX();
        double v5y = v5.getY();
        double v5z = v5.getZ();
        
        //Getting the dimension
        double length = Math.sqrt(Math.pow((v3x-v2x),2)+Math.pow((v3y-v2y),2)+Math.pow((v3z-v2z),2));
        double width = Math.sqrt(Math.pow((v3x-v4x),2)+Math.pow((v3y-v4y),2)+Math.pow((v3z-v4z),2));
        double height = Math.sqrt(Math.pow((v5x-v4x),2)+Math.pow((v5y-v4y),2)+Math.pow((v5z-v4z),2));
        
        //Calculating the volume
        output = length*width*height;
        
        //Rounding
        output = (double) Math.round(output*1000)/1000;
        
        return output;
    }

    @Override
    public ThreeDPoint center()
    {
        ThreeDPoint output;
        
        //In order to find the center of cuboid we have to calculate the average of all
        //xs, ys, and zs
        double averageX = 0;
        double averageY = 0;
        double averageZ = 0;
        
        //This for loop will be responsible of accumulating the sums of coordinates
        for(int i=0;i<vertices.length;i++)
        {
            averageX+= vertices[i].getX();
            averageY+= vertices[i].getY();
            averageZ+= vertices[i].getZ();
        }
        
        //Then after the for loop we have accumulating all the point's sum we just have
        //to divide by 8 to get our center point's coordinate
        averageX = averageX/8;
        averageY = averageY/8;
        averageZ = averageZ/8;
        
        //Rounded points
        averageX = (double) Math.round(averageX*1000)/1000;
        averageY = (double) Math.round(averageY*1000)/1000;
        averageZ = (double) Math.round(averageZ*1000)/1000;
        
        //Storing it into a ThreeDPoint object
        output = new ThreeDPoint(averageX,averageY,averageZ);
        
        return output;
    }

    @Override
    public int compareTo(ThreeDShape o) 
    {
        //This is the volume of this Object
        double thisVolume = this.volume();
        
        //This is the volume of the Object we are comparing to
        double thatVolume = o.volume();
        
        //Same volume thus return 0
        if(thisVolume==thatVolume)
        {
            return 0;
        }
        //If this volume is greater than that volume you comparing to return 1
        else if(thisVolume>thatVolume)
        {
            return 1;
        }
        //If it is not then return -1
        else
        {
            return -1;
        }
    }
    
    //This method will allow us to find the surface area of the Cuboid
    public double surfaceArea()
    {
        //output will be storing the answer
        double output;
        
        //We will use 4 points to find the length, width, and height of the Cuboid
        ThreeDPoint v0 = vertices[0];
        ThreeDPoint v2 = vertices[2];
        ThreeDPoint v3 = vertices[3];
        ThreeDPoint v4 = vertices[4];
        
        double height = ThreeDPoint.findLength(v0,v3);
        double length = ThreeDPoint.findLength(v2,v3);
        double width = ThreeDPoint.findLength(v3,v4);
        
        //Following the formula for the surface area of a rectangular prism
        output = 2*length*height+2*length*width+2*width*height;
        
        output = (double) Math.round(output*1000)/1000;
        
        return output;
    }
    
    //Give a nice String representation of the Cuboid
    public String toString()
    {
        String output = "";
        
        for(ThreeDPoint p:vertices)
        {
            output = output+p+",";
        }
        
        output=output+"\nSurface Area: "+surfaceArea()+"\nVolume: "+volume()+"\n";
        
        return output;
    }
    
    //Create a random cuboid object
    public static Cuboid random()
    {
        //points will be holding the list of random points
        List<ThreeDPoint> points = new ArrayList<ThreeDPoint>();
        
        //output will be the Cuboid that hold our random Cuboid in the end
        Cuboid output;
        
        //We pick an arbitary length, width, and height from 1 to 100
        double length = (int)(Math.random()*100+1);
        double width = (int)(Math.random()*100+1);
        double height = (int)(Math.random()*100+1);
        
        //Arbitary starting vertex from the range -100 to 100
        double randomX = (int)(Math.random()*201-100);
        double randomY = (int)(Math.random()*201-100);
        double randomZ = (int)(Math.random()*201-100);
        
        //Then according to the origin we generate the rest of the seven points with
        //the random length, width, and height to ensure it is a valid Cuboid 
        ThreeDPoint v0 = new ThreeDPoint(randomX,randomY,randomZ);
        ThreeDPoint v1 = new ThreeDPoint(randomX-length,randomY,randomZ);
        ThreeDPoint v2 = new ThreeDPoint(randomX-length,randomY-height,randomZ);
        ThreeDPoint v3 = new ThreeDPoint(randomX,randomY-height,randomZ);
        ThreeDPoint v4 = new ThreeDPoint(randomX,randomY-height,randomZ-width);
        ThreeDPoint v5 = new ThreeDPoint(randomX,randomY,randomZ-width);
        ThreeDPoint v6 = new ThreeDPoint(randomX-length,randomY,randomZ-width);
        ThreeDPoint v7 = new ThreeDPoint(randomX-length,randomY-height,randomZ-width);
        
        //Adding them into the list
        points.add(v0);
        points.add(v1);
        points.add(v2);
        points.add(v3);
        points.add(v4);
        points.add(v5);
        points.add(v6);
        points.add(v7);
        
        //Making the random cuboid object
        output = new Cuboid(points);
        
        //Returning it
        return output;
    }
}
