import java.util.*;

public class Ordering {
    
    //This Comparator will be comparing using the smallest x-coordinate of the TwoDShape
    static class XLocationComparator implements Comparator<TwoDShape>
    {
        @Override
        public int compare(TwoDShape o1, TwoDShape o2)
        {
            //These two values will be holding the smallest x-value of both shapes
            double o1X = 0; 
            double o2X = 0;
            
            //This means that o1 shape is a Circle thus we use Circle's findMinX
            if(o1 instanceof Circle)
            {
                o1X = ((Circle)o1).findMinX();
            }
            //If it is not a Circle then it is a quadrilateral
            else
            {
                o1X = ((Quadrilateral)o1).findMinX();
            }
            
            //This means that o2 shape is a Circle thus we use Circle's findMinX
            if(o2 instanceof Circle)
            {
                o2X = ((Circle)o2).findMinX();
            }
            //If it is not a Circle then it is a quadrilateral
            else
            {
                o2X = ((Quadrilateral)o2).findMinX();
            }
            
            //The first object have a greater x-coordinate thus we return 1
            if(o1X>o2X)
            {
                return 1;
            }
            //The second object have a greater x-coordinate thus we return -1
            else if(o1X<o2X)
            {
                return -1;
            }
            //Meaning the X-coordinates are the same thus we return 0
            else
            {
                return 0;
            }
        }
    }

    //This Comparator will be comparing using the SymmetricTwoDShape's area
    static class AreaComparator implements Comparator<SymmetricTwoDShape>
    {
        @Override
        public int compare(SymmetricTwoDShape o1, SymmetricTwoDShape o2)
        {
            //This means that the first SymmetricTwoDShape have a bigger area thus
            //return 1
            if(o1.area()>o2.area())
            {
                return 1;
            }
            //This means that the second symmetricTwoDShape have a bigger area therefore 
            //we return -1
            else if(o1.area()<o2.area())
            {
                return -1;
            }
            //This means that they hve the same area thus we return 0
            else
            {
                return 0;
            }
        }
    }
    
    //This Comparator will be comparing two ThreeDShape using their surface area
    static class SurfaceAreaComparator implements Comparator<ThreeDShape>
    {
        @Override
        public int compare(ThreeDShape o1, ThreeDShape o2)
        {
            //These two double will be holding the value of surface area of the two 3D shape
            double o1Sa = 0;
            double o2Sa = 0;
            
            //This means that o1 is a Sphere shape therefore we just use Sphere's surfaceArea
            if(o1 instanceof Sphere)
            {
                o1Sa = ((Sphere) o1).surfaceArea();
            }
            //Then it is a Cuboid
            else
            {
                o1Sa = ((Cuboid) o1).surfaceArea();
            }
            
            //This means that o2 is a Sphere shape therefore we just use Sphere's surfaceArea
            if(o2 instanceof Sphere)
            {
                o2Sa = ((Sphere) o2).surfaceArea();
            }
            //Then it is a Cuboid
            else
            {
                o2Sa = ((Cuboid) o2).surfaceArea();
            }
            
            //This means that the first 3D shape have a bigger surfaceArea thus we return 1
            if(o1Sa>o2Sa)
            {
                return 1;
            }
            //This means that the second 3D shape have a bigger surfaceArea thus we return -1
            else if(o1Sa<o2Sa)
            {
                return -1;
            }
            //This means that the two surfaceArea is the same thus we return 0
            else
            {
                return 0;
            }
        }
    }

    public static <A> void copy(Collection<? extends A> source, Collection<A> destination)
    {
        destination.addAll(source);
    }

    public static void main(String[] args)
    {
        //Testing site
        List<TwoDShape> shapes = new ArrayList<>();
        List<SymmetricTwoDShape> symmetricshapes = new ArrayList<>();
        List<ThreeDShape> threedshapes = new ArrayList<>();

        /*
         * uncomment the following block and fill in the "..." constructors to create actual instances. If your
         * implementations are correct, then the code should compile and yield the expected results of the various
         * shapes being ordered by their smallest x-coordinate, area, volume, surface area, etc. 
        */
        
        symmetricshapes.add(new Rectangle(0,0,-4,0,-4,-2,0,-2));
        symmetricshapes.add(new Square(TwoDPoint.ofDoubles(2,0,0,0,0,-2,2,-2)));
        symmetricshapes.add(new Circle(1,2,3));
        symmetricshapes.add(new Circle(2,2,3)); //Added by me
        copy(symmetricshapes, shapes); // note-1 //
        shapes.add(new Quadrilateral(4,4,0,3,4,1,0,0));
        printList(shapes);
        // sorting 2d shapes according to various criteria
        //shapes.sort(new XLocationComparator());
        //symmetricshapes.sort(new XLocationComparator());
        //symmetricshapes.sort(new AreaComparator());

        threedshapes.add(Cuboid.random());
        threedshapes.add(Cuboid.random());
        threedshapes.add(Sphere.random());
        threedshapes.add(Sphere.random());
        //printList(threedshapes);
        //System.out.println("\nSorted afterr\n");
        // sorting 3d shapes according to various criteria
        //Collections.sort(threedshapes); //This default to sorting by volumes
        
        //threedshapes.sort(new SurfaceAreaComparator());
        //printList(threedshapes);
        /*
         * if your changes to copy() are correct, uncommenting the following block will also work as expected note that
         * copy() should work for the line commented with 'note-1' while at the same time also working with the lines
         * commented with 'note-2' and 'note-3'. 
        */

        
        List<Number> numbers = new ArrayList<>();
        List<Double> doubles = new ArrayList<>();
        Set<Square>        squares = new HashSet<>();
        Set<Quadrilateral> quads   = new LinkedHashSet<>();

        doubles.add(new Double(1));
        doubles.add(2.4);
        doubles.add(9.99999);
        doubles.add(-100.9);
        //printList(doubles);
        
        squares.add(new Square(TwoDPoint.ofDoubles(1,1,0,1,0,0,1,0)));
        squares.add(new Square(TwoDPoint.ofDoubles(5,5,0,5,0,0,5,0)));
        //printSet(squares);
        copy(doubles, numbers); // note-2 //
        copy(squares, quads);   // note-3 //
        //printSet(quads);
    }
    
    public static <T> void printList(List<T> list)
    {
        for(T a:list)
        {
            System.out.println(a);
        }
    }
    
    public static <T> void printSet(Set<T> set)
    {
        for(T a:set)
        {
            System.out.println(a);
        }
    }
}
