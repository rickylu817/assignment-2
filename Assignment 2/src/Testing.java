
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Testing
{
    public static void main(String [] args)
    {
        List<TwoDShape> twod = new ArrayList<>();
        List<SymmetricTwoDShape> stwod = new ArrayList<>();
        
        //Adding shape to twod
        twod.add(new Rectangle(TwoDPoint.ofDoubles(0,3,-5,3,-5,-2,0,-2)));
        twod.add(new Square(TwoDPoint.ofDoubles(0,3,-5,3,-5,-2,0,-2)));
        twod.add(new Circle(1,1,5));
        twod.add(new Quadrilateral(-4,4,-4,0,0,-2,0,2));
        twod.add(new Rectangle(TwoDPoint.ofDoubles(6,4,-8,4,-8,-4,6,-4)));
        
        //Adding shape to stwod
        stwod.add(new Rectangle(TwoDPoint.ofDoubles(0,3,-5,3,-5,-2,0,-2)));
        stwod.add(new Square(TwoDPoint.ofDoubles(0,3,-5,3,-5,-2,0,-2)));
        stwod.add(new Circle(1,1,5));
        stwod.add(new Rectangle(TwoDPoint.ofDoubles(6,4,-8,4,-8,-4,6,-4)));
        printList(((Square)twod.get(1)).getSideLengths());
        //Testing
        printList(twod);
        
        System.out.println("-------------End of test 1");
        
        Collections.sort(twod, new Ordering.XLocationComparator());
        printList(twod);
        
        System.out.println("-----------End of test 2");
        
        Collections.sort(stwod, new Ordering.AreaComparator());
        printList(stwod);
        
        System.out.println("-----------End of test 3");
        
    }
    
    public static <T> void printList(List<T> shapes)
    {
        for(T a:shapes)
        {
            System.out.println(a);
        }
    }
    
    public static void printList(double nums[])
    {
        for(double d: nums)
        {
            System.out.println(d);
        }
    }
}