import java.util.List;

public class Rectangle extends Quadrilateral implements SymmetricTwoDShape
{
    public Rectangle(double... vertices)
    {
        super(TwoDPoint.ofDoubles(vertices));
    }
    
    public Rectangle(List<TwoDPoint> vertices)
    {
        super(vertices);
    }
    
    /**
     * The center of a rectangle is calculated to be the point of intersection
     * of its diagonals.
     *
     * @return the center of this rectangle.
     */
    @Override
    public Point center()
    {
        //The output will be a Point
        Point output;
        
        List<TwoDPoint> points = this.getPosition();
        
        //We just need two opposite points to figure out where the diagonals intersect
        //if this is already a rectangle
        TwoDPoint a = points.get(0);
        TwoDPoint c = points.get(2);
        
        //We will use a and b to figure the center of this rectangle.
        //Taking the average value between the x and the y will yield the midpoint
        double outputX = (a.getX()+c.getX())/2;
        double outputY = (a.getY()+c.getY())/2;
        
        outputX = (double) Math.round(outputX*1000)/1000;
        outputY = (double) Math.round(outputY*1000)/1000;
        
        //Creating a new TwoDPoint object for the output
        output = new TwoDPoint(outputX,outputY);
        
        return output;
    }

    @Override
    public boolean isMember(List<? extends Point> vertices)
    {
        //If the given vertices doesn't have 4 Point then we return it is not a memeber
        if(vertices.size()!=4)
        {
            return false;
        }
        
        //We need to get all of the points in order to really check if
        //it is valid to be a rectangle
        TwoDPoint a =(TwoDPoint)vertices.get(0);
        TwoDPoint b =(TwoDPoint)vertices.get(1);
        TwoDPoint c =(TwoDPoint)vertices.get(2);
        TwoDPoint d =(TwoDPoint)vertices.get(3);
        
        //Finding the minor diagonal's middle
        double acx = (a.getX()+c.getX())/2;
        double acy = (a.getY()+c.getY())/2;
        
        //Finding the major diagonal's middle
        double bdx = (b.getX()+d.getX())/2;
        double bdy = (b.getY()+d.getY())/2;
        
        //The diagonals intersect therefore is a quadrilateral
        if(acx==bdx&&acy==bdy)
        {
            //Then we have to also check whether or not the diagonals are the same
            //length
            if(findLength(a,c)==findLength(b,d))
            {
                return true;
            }
            //If they are not the same thus it is not a rectangle
            else
            {
                return false;
            }
        }
        //The diagonals doesn't intersect thus is not a rectangle
        else
        {
            return false;
        }
    }

    @Override
    public double area()
    {
        //This is the output
        double output = 0;
        
        //Getting the list of point using the accessor method
        List<TwoDPoint> points = this.getPosition();
        
        //We only need three points
        TwoDPoint a = points.get(0);
        TwoDPoint b = points.get(1);
        TwoDPoint c = points.get(2);
        
        //Multiply them to findthe area, we also need to round to the 3 decimal places
        //to avoid round off error
        output = findLength(a,b)*findLength(b,c);
        
        output = (double) Math.round(output*1000)/1000;
        
        return output;
    }
    
    @Override
    public String toString()
    {
        String output = "";
        
        List<TwoDPoint> roundedPoints = this.getPosition();
        
        for(TwoDPoint p:roundedPoints)
        {
            output = output+p.toString()+" "+this.getClass().getCanonicalName()+"\n";
        }
        
        output = output+"Area: "+area()+"\n";
        
        return output;
    }
}
