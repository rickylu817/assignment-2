import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

public class Circle implements Positionable, SymmetricTwoDShape
{
    //Instance variables
    private TwoDPoint center;
    private double radius;

    public Circle(double centerx, double centery, double radius)
    {
        this.center = new TwoDPoint(centerx, centery);
        this.radius = radius;
    }

    @Override
    public Point center()
    {
        return center;
    }

    /**
     * Sets the position of the circle to be centered at the first element in
     * the specified list of points.
     *
     * @param points the specified list of points.
     * @throws IllegalArgumentException if the input does not consist of
     * {@link TwoDPoint} instances.
     */
    @Override
    public void setPosition(List<? extends Point> points)
    {
        //This means that the List of points is empty thus nothing to check
        if(points.isEmpty())
        {
            throw new IllegalArgumentException();
        }
        
        //If we are out here that means there is at least one point to check. we therefore take
        //the first Point object and see if it is a TwoDPoint
        Point firstPoint = points.get(0);
        
        //This is checking if the firstPoint in the List is a TwoDPoint object
        //if it is then good, we set center to it
        if(firstPoint instanceof TwoDPoint)
        {
            center = (TwoDPoint)firstPoint;
        }
        //If it is not then we throw an illegalArgumentExeption
        else
        {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public List<? extends Point> getPosition()
    {
        return Collections.singletonList(center);
    }

    public void setRadius(double r)
    {
        this.radius = r;
    }

    public double getRadius()
    {
        return radius;
    }
   
    @Override
    public int numSides()
    {
        return 0; // even though it really should be positive infinity
    }

    @Override
    public boolean isMember(List<? extends Point> centers)
    {
        return centers.size() == 1 && radius > 0;
    }

    @Override
    public double area()
    {
        return Math.PI * Math.pow(radius, 2);
    }
    
    //This method will allow us to find the minimum value of this shape
    public double findMinX()
    {
        BigDecimal x = BigDecimal.valueOf(center.getX());
        BigDecimal rad = BigDecimal.valueOf(radius);
        
        BigDecimal output = x.subtract(rad);
        
        return output.doubleValue();
    }
    
    public String toString()
    {
        String output = center+" Circle\n";
        
        return output;
    }
}
