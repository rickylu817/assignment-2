import java.util.Arrays;
import java.util.List;

public class Quadrilateral implements Positionable, TwoDShape
{
    private final TwoDPoint[] vertices = new TwoDPoint[4];

    public Quadrilateral(double... vertices)
    {
        this(TwoDPoint.ofDoubles(vertices));
    }

    public Quadrilateral(List<TwoDPoint> vertices)
    {
        int n = 0;
        for (TwoDPoint p : vertices)
        {
            this.vertices[n++] = p;
        }
        if(!isMember(vertices))
        {
            throw new IllegalArgumentException(String.format("Invalid set of vertices specified for %s",
                    this.getClass().getCanonicalName()));
        }
    }

    /**
     * Given a list of four points, adds them as the four vertices of this
     * quadrilateral in the order provided in the list. This is expected to be a
     * counterclockwise order of the four corners.
     *
     * @param points the specified list of points.
     * @throws IllegalStateException if the number of vertices provided as input
     * is not equal to four.
     */
    @Override
    public void setPosition(List<? extends Point> points)
    {
        //If the points given have is not a size of 4 therefore it is not a valid set of
        //points
        if(points.size()!=4)
        {
            throw new IllegalStateException("The number of vetices provided is not four");
        }
        
        //If we are out here then that means there is 4 vertices for us to work with
        //Thus we set each of ther vertices to the given points
        for(int i=0;i<points.size();i++)
        {
            vertices[i]=(TwoDPoint)points.get(i);
        }
    }

    @Override
    public List<TwoDPoint> getPosition()
    {
        return Arrays.asList(vertices);
    }

    /**
     * @return the lengths of the four sides of the quadrilateral. Since the
     * setter {@link Quadrilateral#setPosition(List)} expected the corners to be
     * provided in a counterclockwise order, the side lengths are expected to be
     * in that same order.
     */
    protected double[] getSideLengths()
    {
        //The output will have 4 entries because it is 4 sides of length
        double output[] = new double[4];
        
        double s01 = findLength(vertices[0],vertices[1]);
        s01 = (double) Math.round(s01*1000)/1000;
        
        double s12 = findLength(vertices[1],vertices[2]);
        s12 = (double) Math.round(s12*1000)/1000;
        
        double s23 = findLength(vertices[2],vertices[3]);
        s23 = (double) Math.round(s23*1000)/1000;
        
        double s30 = findLength(vertices[3],vertices[0]);
        s30 = (double) Math.round(s30*1000)/1000;
        
        //Storing the output
        output[0] = s01;
        output[1] = s12;
        output[2] = s23;
        output[3] = s30;
        
        //Returning the sides length of array of doubles
        return output;
    }
    
    public double findLength(TwoDPoint a, TwoDPoint b)
    {
        double output = 0;
        
        double ax = a.getX();
        double ay = a.getY();
        
        double bx = b.getX();
        double by = b.getY();
        
        output = Math.sqrt(Math.pow(ax-bx,2)+Math.pow(ay-by,2));
        
        return output;
    }

    @Override
    public int numSides()
    {
        return 4;
    }

    @Override
    public boolean isMember(List<? extends Point> vertices)
    {
        return vertices.size() == 4;
    }
    
    public String toString()
    {
        String output = "";
        
        List<TwoDPoint> roundedPoints = this.getPosition();
        
        for(TwoDPoint p:roundedPoints)
        {
            output = output+p.toString()+" "+this.getClass().getCanonicalName()+"\n";
        }
        
        return output;
    }
    
    //Allow us to find the smallest x value of the quadrilateral, rectangle, or square
    public double findMinX()
    {
        //This is our output, we assume that the first point in thevertices is the smallest X
        //Then we use the for loop to check over the rest of the points
        double output = vertices[0].getX();
        
        for(int i=1;i<vertices.length;i++)
        {
            //This means that the xValue we have is not the smallest, therefore
            //We switch to the one that is the smallest
            if(output>vertices[i].getX())
            {
                output=vertices[i].getX();
            }
        }
        
        //If we are outside of the for loop that means that output is the smallest
        //X value of the vertices, thus we return it
        return output;
    }
}
